import React from 'react'
import Header from '../components/Header'
import Invoice from '../components/Invoice'

const Invoices = () => {
  const invoices = [1, 2, 3, 4, 5, 6]
  return (
    <div className="md:pl-16 w-full sm:w-10/12  md:w-10/12 lg:w-8/12 xl:5/12 mx-auto">
      <Header />
      <div className="space-y-4 px-2">
        {
          invoices.map(invoice => (
            <Invoice key={invoice} />
          ))
        }
      </div>
    </div>
  )
}

export default Invoices