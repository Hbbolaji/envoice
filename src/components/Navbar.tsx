import React from 'react'
import {FiHome, FiSun} from 'react-icons/fi'
import profile from '../assets/profile.jpg'

const Navbar = () => {
  return (
    <div className="bg-gray-900 md:fixed w-full md:w-16 h-16 md:h-screen flex flex-row md:flex-col">
      <div className="bg-indigo-500 text-2xl w-16 md:h-16 flex justify-center items-center cursor-pointer">
        <FiHome />
      </div>
      <div className="text-2xl flex justify-center items-center flex-grow"></div>
      <div className="text-2xl w-16 md:h-16 flex justify-center items-center cursor-pointer">
        <FiSun />
      </div>
      <div className="text-2xl w-20 md:w-full md:h-20 flex justify-center items-center cursor-pointer border-l-2 md:border-l-0 md:border-t-2 border-gray-700">
        <img className="h-12 w-12 object-center object-cover rounded-full" src={profile} alt="profile" />
      </div>
    </div>
  )
}

export default Navbar
