import React from 'react'
import { GoPrimitiveDot } from 'react-icons/go'
import { FiChevronRight } from 'react-icons/fi'

const Invoice = () => {
  return (
    <div className="box-border w-full px-6 md:pl-0 pr-3 hover:bg-gray-800 bg-gray-900 text-gray-300 py-6 rounded-md flex justify-around items-center">
      <div className="flex flex-col justify-between md:flex-row md:justify-around md:items-center w-full">
        <h1 className="text-xl font-semibold mb-6 md:mb-0">#RT3080</h1>
        <h1 className="mb-2 md:mb-0">Due 19 Aug 2021</h1>
        <h1 className="hidden md:block">Jensen Huang</h1>
        <h1 className="text-xl font-semibold">$1,800.90</h1>
      </div>
      <div className="flex flex-col justify-between items-end md:flex-row md:justify-around md:items-center w-full md:w-auto self-stretch pr-6 md:pr-0">
        <h1 className="text-lg md:hidden flex-grow-1 md:flex-shrink">Jensen Huang</h1>
        <span className="flex items-center space-x-3">
          <span className="flex text-xl md:text-sm items-center space-x-2 py-2 md:py-1 font-semibold bg-red-300 text-red-800 px-8 md:px-6 rounded-lg">
            <GoPrimitiveDot />
            Paid
          </span>
          <span className="hidden md:inline-block cursor-pointer text-indigo-500 text-semibold text-xl">
            <FiChevronRight />
          </span>
        </span>
      </div>
    </div>
  )
}

export default Invoice
