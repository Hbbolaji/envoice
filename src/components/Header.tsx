import React from 'react'
import {FiPlus} from 'react-icons/fi'


const Header = () => {
  return (
    <div className="py-6 px-2 md:py-12 flex items-center w-full space-x-2 md:space-x-12">
      <div>
        <h1 className="text-xl md:text-3xl text-gray-50 tracking-wide font-bold">Invoices</h1>
        <p className="text-xs">There are 7 total invoices</p>
      </div>
      <div className="flex-grow"></div>
      <div>
        <select className="bg-gray-800 focus:outline-0" name="status" id="status">
        <option>Status</option>
          <option value="paid">Paid</option>
          <option value="pending">Pending</option>
          <option value="draft">Draft</option>
        </select>
      </div>
      <div>
        <button className="w-20 md:w-32 py-2 font-semibold text-sm bg-indigo-500 rounded-3xl flex items-center justify-around">
          <span className="bg-gray-50 text-indigo-500 p-1 rounded-full font-bold text-xl"><FiPlus/></span>
          New <span className="hidden md:inline">Invoice</span>
        </button>
      </div>
    </div>
  )
}

export default Header
