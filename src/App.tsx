import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Invoices from './pages/Invoices'
import Navbar from './components/Navbar'
import './styles/main.css'


function App() {
  return (
    <Router>
        <div className="relative min-h-screen text-white bg-gray-800 flex flex-col md:flex-row">
          <Navbar />
          <div className="flex-grow">
            <Switch>
              <Route path="/" component={Invoices} />
            </Switch>
          </div>
        </div>
      </Router>
  );
}

export default App;
